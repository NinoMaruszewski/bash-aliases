#!/bin/bash

echo "Nino Maruszewski's bash aliases!"

bashrc_init_statement=$(
    cat <<EOF
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
EOF
)

echo -e "\nChecking bashrc"
# check for refernece to ~/.bash_aliases in .bashrc
if [[ -z $(grep .bash_aliases ~/.bashrc) ]]; then
    # if none, add loading of .bash_aliases
    echo "Adding ref to bashrc"
    echo -e "\n$bashrc_init_statement" >>~/.bashrc
fi

# now copy bash_aliases file, pipe it to ~/.bash_aliases
echo -e "\nCopying aliases"
curl --progress-bar --location "https://bitbucket.org/NinoMaruszewski/bash-aliases/raw/master/bash_aliases.sh" >~/.bash_aliases

# all done!
echo -e "\n\n"
echo    "          _ _   _____                   "
echo    "    /\   | | | |  __ \                  "
echo    "   /  \  | | | | |  | | ___  _ __   ___ "
echo -e "  / /\ \ | | | | |  | |/ _ \| '_ \ / _ \\"
echo    " / ____ \| | | | |__| | (_) | | | |  __/"
echo    "/_/    \_\_|_| |_____/ \___/|_| |_|\___|"
