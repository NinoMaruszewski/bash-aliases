#!/bin/bash

tldr_theme=$(
    cat <<EOF
# tldr stuff
complete -W "$(tldr 2>/dev/null --list)" tldr
export TLDR_HEADER='bold'
export TLDR_QUOTE='default'
export TLDR_DESCRIPTION='green'
export TLDR_CODE='red'
export TLDR_PARAM='cyan'
EOF
)

# Check to make sure not installed already
if [[ -n $(grep tldr ~/.bashrc) ]]; then
    # If none, then tldr already installed, so exit
    echo "TLDR already installed, exiting."
    exit 1
fi

# Curl to /bin
echo -e "Curling to /bin and making executable.\n"
curl -o /bin/tldr https://raw.githubusercontent.com/raylee/tldr/master/tldr

# Make executable
chmod +x /bin/tldr

# Insert into bashrc
echo -e "\nInstallation finished, now adding autocomplete and theme to bashrc."
echo -e "\n$tldr_theme" >>~/.bashrc

# all done!
echo -e "\n\n"
echo    "          _ _   _____                   "
echo    "    /\   | | | |  __ \                  "
echo    "   /  \  | | | | |  | | ___  _ __   ___ "
echo -e "  / /\ \ | | | | |  | |/ _ \| '_ \ / _ \\"
echo    " / ____ \| | | | |__| | (_) | | | |  __/"
echo    "/_/    \_\_|_| |_____/ \___/|_| |_|\___|"
