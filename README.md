# Project Title

## Table of Contents

- [About](#about)
- [Installation](#installation)
- [License](./LICENSE)

## About

My bash_aliases and an injector script

## Installation
Run this:
```shell
$ curl -sL "https://bit.ly/2DKd0Yh" | bash -
```

For tldr:
```shell
$ curl -sL "https://bitbucket.org/NinoMaruszewski/bash-aliases/raw/master/install_tldr.sh" | sudo -E bash -
```
